<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'wp_metro');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'def456..');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'A3>,?y72@Ieje#XHt^auJ.3?HbB&kmknr#[X^2eayCtCc,B|>&s{{m~+CY=a|nFV');
define('SECURE_AUTH_KEY', '*-a*zsclP{8(iS(ft]:(54luMb?fIPB%`sJKQ8&c^w_{&!+4|8f_,8l9KFaA8>jF');
define('LOGGED_IN_KEY', ' 4 4pu.5i:s7si53HiM+zu4<-4[e9.cq|F-K-0ujsMwIQei^hn{2 C]U5%>r>(F6');
define('NONCE_KEY', 'HHY1d|0*36]mQ|7d_yR-.;D>&z_V/%/!TgLuLbIHU -!DcHHF^`av|<>QCFF)|g:');
define('AUTH_SALT', 'BN)Frahs:?`+C) X0Bi6E7%)hKJ68:>]qX{vKNW0&?diYn;>3C+e6^?`hFw6SC*t');
define('SECURE_AUTH_SALT', 'R@;T/qX;c+i6_$4lb?HD}pO&E$x]<6[>x+a>%~8w3icVZwfM>,00V{([XG|`S5A%');
define('LOGGED_IN_SALT', ':1)-%2Z&qbu~A&qZJHL0~A+](h0{Gj-S.dp[!^j/z=D~M2t|&#Q1}$-!L1uVR6Ut');
define('NONCE_SALT', 'PK|smQdCjvK^y=#9;E;&ZznH/w$7c#M6|+{WC@gxN%Eu8?:E9bDzb}wBr|=caH,j');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


